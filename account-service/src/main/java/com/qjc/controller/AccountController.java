/*
 * Copyright (C) 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qjc.controller;

import com.qjc.mapper.AccountMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @ClassName: AccountController
 * @Description: 账户服务：扣钱
 * @Author: qiaojiacheng
 * @Date: 2021/4/13 4:20 下午
 */
@RestController
public class AccountController {

    @Resource
    AccountMapper accountMapper;

    @RequestMapping(value = "/account", method = RequestMethod.GET, produces = "application/json")
    public String account(String userId, int money) {
        accountMapper.updateMoney(userId, money);
        return "SUCCESS";
    }

}
