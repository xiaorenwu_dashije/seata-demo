package com.qjc.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @ClassName: OrderService
 * @Description: 订单服务：保存订单信息
 * @Author: qiaojiacheng
 * @Date: 2021/4/13 4:40 下午
 */
@FeignClient(value = "orderService", url = "http://127.0.0.1:18083")
@Service
public interface OrderService {

    @RequestMapping(path = "/order", method = RequestMethod.GET)
    String order(@RequestParam("userId") String userId, @RequestParam("commodityCode") String commodityCode, @RequestParam("orderMoney") int orderMoney);

}
