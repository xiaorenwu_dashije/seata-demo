/*
 * Copyright (C) 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.qjc.controller;

import com.qjc.mapper.StorageMapper;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @ClassName: StorageController
 * @Description: 仓储服务：扣库存
 * @Author: qiaojiacheng
 * @Date: 2021/4/13 4:02 下午
 */
@RestController
public class StorageController {

    @Resource
    StorageMapper storageMapper;

    /**
     * 库存减1
     *
     * @param
     * @return
     * @author qiaojiacheng
     * @date 2021/4/13 4:16 下午
     */
    @RequestMapping(value = "/storage/{commodityCode}", method = RequestMethod.GET, produces = "application/json")
    public String echo(@PathVariable String commodityCode) {
        storageMapper.updateCountByCommodityCode(commodityCode);
        //操作多张表依然可以控制事务
//        storageMapper.updateCountByCommodityCode1(commodityCode);
        return "SUCCESS";
    }
}
