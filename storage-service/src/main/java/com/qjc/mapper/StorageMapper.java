package com.qjc.mapper;

import com.qjc.entity.Storage;
import com.qjc.entity.StorageExample;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface StorageMapper {
    long countByExample(StorageExample example);

    int deleteByExample(StorageExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Storage record);

    int insertSelective(Storage record);

    List<Storage> selectByExample(StorageExample example);

    Storage selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Storage record, @Param("example") StorageExample example);

    int updateByExample(@Param("record") Storage record, @Param("example") StorageExample example);

    int updateByPrimaryKeySelective(Storage record);

    int updateByPrimaryKey(Storage record);

    @Update("UPDATE storage_tbl set count = count-1 WHERE commodity_code = #{commodityCode}")
    void updateCountByCommodityCode(@Param("commodityCode") String commodityCode);

    @Update("UPDATE storage_tbl_1 set count = count-1 WHERE commodity_code = #{commodityCode}")
    void updateCountByCommodityCode1(@Param("commodityCode") String commodityCode);

}